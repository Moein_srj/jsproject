$(document).ready(function() {
    $("#fadeOut").on("click", function() {
        $(".box").fadeOut("slow", function() {
            alert('faded... ')
        });
    });
    $("#fadeIn").on("click", function() {
        $(".box").fadeIn(3000);
    });

    $("#fadeToggle").on("click", function() {
        $(".box").fadeToggle("fast");
    });

    $("#slideDown").on("click", function() {
        alert('Please wait...');
    });
    $("#slideDown").on("click", function() {
        $(".box").slideDown(2800);
    });
    $("#slideUp").on("click", function() {
        $(".box").slideUp('slow');
    });

    $("#slideToggle").on("click", function() {
        $(".box").slideToggle('fast');
    });
    $("#stop").on("click", function() {
        $(".box").stop();
    });

    $('#right').on('click', function() {
        $('.box').animate({
            right:"0"
        });
    })
    $('#left').on('click', function() {
        $('.box').animate({
            left: '0'
        });
    })
    $('#up').on('click', function() {
        $('.box').animate({
            top: '0'
        });
    })
    $('#down').on('click', function() {
        $('.box').animate({
            down: '0'
        });
    })

});